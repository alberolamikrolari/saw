----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:54:09 11/04/2013 
-- Design Name: 
-- Module Name:    main - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DAC_Driver is

    Port(	CLK 			: in  std_logic;
				Reset		: in  std_logic;
				Leds		: out std_logic_vector(7 downto 0);
				
				DAC_CLK		: out std_logic;
				DAC_MOSI	: out std_logic;
				DAC_FS		: out std_logic;
				DAC_CS		: out std_logic);
			
end DAC_Driver;

architecture Behavioral of DAC_Driver is
	
	signal DAC_CLK_tmp		: std_logic;

	signal DAC_Contador 	: std_logic_vector(4 downto 0);						-- Contador para el prescaler del DAC, 
																				-- a comparar con la constante 
																				-- "DAC_Prescaler"
	
	signal DAC_DATA_Shift	: std_logic_vector(16 downto 0);					-- Dato registro desplazamiento
																				-- Ver formato en el datasheet del
																				-- TLV5616, pag 12 "data format".
																						
	signal DAC_Estado		: std_logic_vector(4 downto 0);						-- Estado de la maquina de estados
																				-- del driver del DAC (17 estados,
																				-- del 0 al 16)
	
	constant DAC_Prescaler 	: std_logic_vector(4 downto 0) := "11000"; 			-- La frecuencia del SPI clock ser� de 
																				-- 50MHz / 50 = 1MHz
	
	signal DAC_Data 		: std_logic_vector(11 downto 0);
	
	signal Saw_Contador		: std_logic_vector(15 downto 0);					-- Contador para el prescaler del generador de diente de sierra,
																				-- a comparar con la constante "Saw_Prescaler"
	signal Saw_Out			: std_logic_vector(11 downto 0);

	constant Saw_Prescaler	: std_logic_vector(15 downto 0) := x"0055";			-- Prescaler del generador del diente de sierra
	constant Saw_Max 		: std_logic_vector(11 downto 0) := x"FF5";			-- Valor maximo del diente de sierra
	constant Saw_Min 		: std_logic_vector(11 downto 0) := x"005";			-- Valor minimo del diente de sierra

begin

	Prescaler : process(clk, reset)
	
	begin
	
		if (Reset = '0') then
		
			DAC_Contador	<= (others => '0');
			DAC_Estado		<= (others => '0');
			DAC_CLK_tmp 	<= '0';
			DAC_Data_Shift <= "1" & "0101" & DAC_Data; 
			
		elsif (clk'event and clk = '1') then
		
			DAC_Contador <= DAC_Contador + 1;
				
			if (DAC_Contador = DAC_Prescaler) then
			
				if (DAC_CLK_tmp = '0') then										-- Si va a haber un flanco ascendente en DAC_CLK 
																				-- avanzamos un estado en la maquina de estados.
					if (DAC_Estado = "10000") then								-- Si estamos en el estado 16 (avanzariamos al 17, 
																				-- que no existe), reseteamos
						DAC_Estado <= "00000";									-- el contador de estados (DAC_Estado),
						DAC_FS <= '1';											-- ponemos a '1' la salida FS (Frame Sync del DAC), 
						DAC_Data_Shift <= "1" & "0101" & DAC_Data;				-- y cargamos el nuevo valor a convertir.
																				-- Formato: 1 bit a '1' durante el tiempo de Frame Sync +
																				-- + 4 bits de configuracion + 12 bits de dato
					else
					
						DAC_Estado <= DAC_Estado + 1;							-- Si no hemos llegado al estado 16, avanzamos un estado,
						DAC_FS <= '0';											-- mantenemos FS a '0' (Frame Sync del DAC)
						DAC_Data_Shift <= DAC_Data_Shift(15 downto 0) & '0';	-- y desplazamos DAC_Data_Shift
																	
					end if;
				
				end if;	
					
				DAC_Contador <= (others => '0');
				DAC_CLK_tmp <= not(DAC_CLK_tmp);
					
			end if;
				
		end if;
							
	end process;
	
	
	Saw_Generator : process (CLK, Reset)
	
	begin
	
		if (Reset = '0') then
		
			Saw_Contador	<= (others => '0');
			Saw_Out			<= (others => '0');
			
		elsif (CLK'event and CLK = '1') then
		
			if (Saw_Contador = Saw_Prescaler) then
			
				Saw_Contador	<= (others => '0');
				
				if (Saw_Out = Saw_max) then
				
					Saw_Out <= Saw_Min;					
					
				else
				
					Saw_Out	<= Saw_Out + 1; 
					
				end if;
			
			else

				Saw_Contador <= Saw_Contador + 1;
			
			end if;
			
		end if;
		
		DAC_Data <= Saw_Out;
		
	end process;
	
	

	
DAC_CS	<= '1';
DAC_MOSI <= DAC_Data_Shift(16);
DAC_CLK	<=	DAC_CLK_tmp;


Leds(0) <= Reset;
Leds(7 downto 1) <= (others => '0');

end Behavioral;

